<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;
use common\models\Profile;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $forename;
    public $surname;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'email'],
            ['username', 'string', 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['email', 'compare', 'compareAttribute' => 'username'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['forename', 'required'],
            ['forename', 'string', 'max'=>255],

            ['surname', 'required'],
            ['surname', 'string', 'max'=>255],

        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app','Email'),
            'forename' => Yii::t('app','First Name'),
            'surname' => Yii::t('app','Last Name'),
            'email' => Yii::t('app','Repeat Email'),
            'password' => Yii::t('app','Password'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->save();
        $profile = new Profile();
        $profile->user_id = $user->id;
        $profile->forename = $this->forename;
        $profile->surname = $this->surname;
        $profile->role = Profile::USR_PUBLIC;
        $profile->save();
        return $this->sendEmail($user, $profile);

    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user, $profile)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user,'profile'=>$profile]
            )
            ->setFrom('support@backtestonline.com')
            ->setBcc('support@backtestonline.com')
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
