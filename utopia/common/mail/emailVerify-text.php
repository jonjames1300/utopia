<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $profile common\models\Profile */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
?>
Hello <?= $profile->forename . " " . $profile->surname ?>,

Follow the link below to verify your email:

<?= $verifyLink ?>

The Backtest Dev Team