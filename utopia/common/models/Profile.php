<?php
namespace common\models;

use yii\db\ActiveRecord;

class Profile extends ActiveRecord{

    const USR_ADMIN = 1;
    const USR_PUBLIC = 2;

    public static function tableName()
    {
        return '{{%profile}}';
    }

}